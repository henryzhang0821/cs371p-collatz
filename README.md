# CS371p: Object-Oriented Programming Collatz Repo

* Name: Henry Zhang

* EID: hz3953

* GitLab ID: 5344777

* HackerRank ID: defenestrate667

* Git SHA: 479ba75a3f5070234bbcc55e4ec01729241b02c9

* GitLab Pipelines: https://gitlab.com/henryzhang0821/cs371p-collatz/pipelines

* Estimated completion time: 5

* Actual completion time: 7

* Comments: N/A
