// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <map>
#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}


int find_cycle(int i) {
    int cycles = 0;
    while(i != 1) {
        if(i%2 == 0) {
            i = i/2;
        }
        else {
            i = (3*i + 1)/2;
            cycles += 1;
        }
        cycles++;
    }
    return cycles;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    //pre-condition assertions
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);


    std::map<int, int> cache;

    if(i > j) {
        int temp = j;
        j = i;
        i = temp;
    }

    //optimization: make i half of j if i is less than half of j
    int temp = j/2;
    if(temp > i)
        i = temp;
    int currentCycle = 0;
    int maxCycle = 1;
    int cycles = 1;

    while(i <= j) {
        cycles = 1;

        if(cache.count(i) == 0) {
            int index = i;
            while(index > 1) {
                if(cache.count(index) != 0) {
                    cycles += cache[index] - 1;
                    break;
                }
                //if number is odd, can take two steps at once
                if(index%2 != 0) {
                    index = index + (index>>1) + 1;
                    cycles += 2;
                }
                else {
                    index = index >> 1;
                    cycles += 1;
                }
            }
        }
        //checks the cache for cycles already went through
        else {
            cycles = cache[i];
        }
        cache[i]= cycles;
        i += 1;
        maxCycle = std::max(maxCycle, cycles);
    }
    //assert that the maxCycle is at least 1
    assert(maxCycle > 0);

    return maxCycle;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
